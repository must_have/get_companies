import re
import requests
from bs4 import BeautifulSoup
from lxml import html
import time
import datetime
import urllib.request
import csv
import re
from IB import get_historical
import getDB
import pandas as pd
from sqlalchemy import create_engine

LINK = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={}&apikey=YM9DLICFC8OKM4Q0.&datatype=csv&outputsize=full'

MAIN_LIST = []

ALL_SYMBOLS = list()


def txt_reader(file_obj):
    """
    Read a csv file
    """
    flag_test = 0

    reader = csv.reader(file_obj)
    for row in reader:
        print(row[0])
        symbol = row[0]
        ALL_SYMBOLS.append(symbol)
        df = get_historical.getHistoricalData(symbol, '4 Y')
        df_to_mysql(df)


def df_to_mysql(df):
    sqlEngine = create_engine('sqlite:///historical_data.db')
    dbConnection = sqlEngine.connect()
    frame = df.to_sql("userVitals", dbConnection, if_exists='append')


def txt_writer(all_data_symbol):
    DB = getDB.mySqlConnect()
    cursor = DB.cursor()
    cursor.execute("""TRUNCATE data""")
    for line in all_data_symbol:
        cursor.execute('insert into `data`(`company`,`date`,`open`,`high`,`low`,`close`,volume) values ("%s","%s",'
                       '"%s","%s","%s","%s","%s") '
                       % (line[0], line[1], line[2], line[3], line[4], line[5], line[6]))
        # company, date,close
    cursor.close()


def get_stocks():
    txt_path = "ALL_SYMBOL.txt"
    with open(txt_path, "r") as f_obj:
        txt_reader(f_obj)
    txt_writer(MAIN_LIST)
