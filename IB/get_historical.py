import os
from time import sleep

from ib_insync import *
from lxml import etree


#  get datasummary and historical data by each company from Interactive Brokers
def getDataSummary(symbol):
    ib = IB()
    ib.connect('127.0.0.1', 7496, clientId=2)
    contract = Stock(symbol, 'SMART', 'USD', primaryExchange='NASDAQ')
    return ib.reqFundamentalData(contract, 'ReportsFinSummary')


def getHistoricalData(symbol,duration = '1 Y'):
    ib = IB()

    connect_flag = True
    while connect_flag:
        try:

            ib.connect('127.0.0.1', 7496,1)
            connect_flag = False
        except:
            sleep(10)
            connect_flag = True
    connect_flag = True
    contract = Stock(symbol, 'SMART', 'USD', primaryExchange='NASDAQ')
    bars = ib.reqHistoricalData(contract, endDateTime='', durationStr=duration,
                                        barSizeSetting='1 day', whatToShow='TRADES', useRTH=True)
    ib.disconnect()
    df = util.df(bars)
    df['company'] = symbol
    return df


# def parse_xml(xml_data, symbol):
#     DB = getDB.mySqlConnect()
#     tags = []
#     date = []
#     period = []
#     value = []
#     root = etree.fromstring(xml_data)
#     for appt in root.getchildren():
#         for elem in appt.getchildren():
#             if elem.get('reportType') == 'A' and elem.get('period') == '3M':
#                 tags.append(elem.tag)
#                 date.append(elem.get('asofDate'))
#                 period.append(elem.get('period'))
#                 value.append(elem.text)
#             if elem.get('reportType') is None:
#                 continue
#     cursor = DB.cursor()
#     i = 0
#     for data in tags:
#         cursor.execute("insert into datasummary(`tag`,`value`,`date`,`period`,`company`) "
#                        "VALUES('%s','%s','%s','%s','%s') " % (tags[i], value[i], date[i], period[i], symbol))
#         i += 1
#
#
# def data():
#     DURATION = '6 Y'
#
#     DB = getDB.mySqlConnect()
#     cursor = DB.cursor()
#     cursor.execute('truncate historical')
#     cursor.execute('select symbol from company')
#     count = cursor.rowcount
#     half = 100 / count
#     percent = 0
#     for symbol in cursor:
#         os.system('cls')
#         print('Downloading:' + str(round(percent, 2)) + '%')
#         df = getHistoricalData(symbol=symbol[0], duration=DURATION)
#         percent += half
#         i = 0
#         cursors = DB.cursor()
#         try:
#             for data in df['close']:
#                 cursors.execute(
#                     "insert into historical(`company`,`open`,`high`,`low`,`close`,`date`) VALUES "
#                     "('%s','%s','%s','%s','%s','%s')"
#                     % (symbol[0], df['open'][i], df['high'][i], df['low'][i], df['close'][i], df['date'][i]))
#                 i += 1
#         except:
#             print('Error data in get_historical.Data()')
#             continue
#
#
# def datasummary():
#     DB = getDB.mySqlConnect()
#     cursor = DB.cursor()
#     cursor.execute('truncate datasummary')
#     cursor.close()
#     cursor = DB.cursor()
#     cursor.execute('select symbol from company')
#     count = cursor.rowcount
#     half = 100 / count
#     percent = 0
#     for company in cursor:
#         print(company[0])
#         percent += half
#         try:
#             ib_data = getDataSummary(symbol=company[0])
#             parse_xml(ib_data, symbol=company[0])
#         except:
#             print('This company {} isn`t exists here'.format(company[0]))
